package com.jithin.cowin;

import java.net.InetAddress;
import java.util.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.mail.Message.RecipientType;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

public class EmailUtil {
	public int sendMail() {
		try{
			final String sender = "jithin.kott@gmail.com";
			String recipient = "kv.jithin616@gmail.com";
			String ccRecipient = "venukalathil11@gmail.com";
			String bccRecipient = null;
			String serverAddress = "smtp.gmail.com";
			String subject = "Vaccination available";
			String emailContent = "Vaccination available for 680652 in this week.";

			if ((recipient == null || recipient.equals("")) && (ccRecipient == null || ccRecipient.equals(""))) {
				return 1;
			}

			try {
				final String PROTOCOL = "smtp";
				String hostName = null;
				Properties properties = System.getProperties();
				MimeBodyPart bodyPart1 = new MimeBodyPart();
				MimeBodyPart bodyPart2 = null;
				MimeBodyPart bodyPart3 = null;
				MimeMultipart multipart = new MimeMultipart();
				MimeMessage message = null;
				Session session = null;
				properties.put("mail.smtp.host", serverAddress);
				properties.put("mail.smtp.port", "587");
				properties.put("mail.smtp.auth", "true");
				properties.put("mail.smtp.starttls.enable", "true");
				properties.put("mail.smtp.user", sender);

				session = Session.getInstance(properties, new javax.mail.Authenticator() {
				    protected PasswordAuthentication getPasswordAuthentication() {
				        return new PasswordAuthentication(sender, "ocrkotts123");
				    }
				});
				
				/* session = Session.getDefaultInstance(properties); */
				message = new MimeMessage(session);
				message.setFrom(new InternetAddress(sender, "Jithin"));
				message.setSentDate(new Date());

				if (recipient != null && !recipient.trim().equals("")) {
					message.addRecipients(RecipientType.TO,
							InternetAddress.parse(recipient.replaceAll(";", ",")));
				}
				if (ccRecipient != null && !ccRecipient.trim().equals("")) {
					message.addRecipients(RecipientType.CC,
							InternetAddress.parse(ccRecipient.replaceAll(";", ",")));
				}
				if (bccRecipient != null && !bccRecipient.trim().equals("")) {
					message.addRecipients(RecipientType.BCC,
							InternetAddress.parse(bccRecipient.replaceAll(";", ",")));
				}
				boolean isServerAddress = true;
				try {
					if(serverAddress != null && !serverAddress.trim().equals("")) {
						hostName = serverAddress;

					}
				} catch (Exception e) {
					System.out.println("Error "+e);
				}
				if (subject != null && !subject.trim().equals("")) {
					if (subject.contains("Regulatory Data Update")
							|| subject.contains("License Screening Data Update")
							|| subject.contains("Invalid credentials for CDR")) {
						if (isServerAddress) {
							hostName = hostName + "-" + InetAddress.getLocalHost().getHostName();
						}
					}
					message.setSubject("[" + hostName + "] - " + subject.replaceAll("[\\r\\n ]", " "));
				} else {
					message.setSubject("[" + hostName + "]");
				}
				if (emailContent != null && !emailContent.trim().equals("")) {
					bodyPart1.setDataHandler(
							new DataHandler(new ByteArrayDataSource(emailContent, "text/html")));
					multipart.addBodyPart(bodyPart1);
				}

				System.out.println("mail debug 1 recipient="+recipient);
				System.out.println("mail debug 2 date time="+new Date());
				System.out.println("mail debug 3 message="+message);

				message.setContent(multipart);

				if (properties.get("mail.smtp.auth").equals("true")) {
					Transport transport = session.getTransport(PROTOCOL);
					transport.connect(serverAddress, sender,
							"ocrkotts123");
					transport.sendMessage(message, message.getAllRecipients());
					transport.close();
				} else {
					Transport.send(message);
				}

			} catch (Exception e) {
				System.out.println("Error "+e);
			}
		}catch(Exception e){
			System.out.println("Error "+e);
		}
		return 0;
	}
}
