package com.jithin.cowin;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONObject;

public class LoadSlots {

	public static void main(String[] args) throws IOException, InterruptedException {	
		var url = "https://cdn-api.co-vin.in/api/v2/appointment/sessions/public/findByPin?pincode=680652&date=";
		boolean stopThread=true;
		
		 try {
		        while (stopThread) {
		            Thread.sleep(60 * 1000);
			         for(int j=1;j<7;j++) {   
			            Calendar calendar = Calendar.getInstance();
			    		calendar.add(Calendar.DATE, j);
			    		Date date = calendar.getTime();  
			    		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");  
			    		String strDate = dateFormat.format(date);  
			    		var urlOrg=url.concat(strDate);
			    		System.out.println(urlOrg);
			    		HttpRequest request = HttpRequest.newBuilder()
			    				.header("Content-Type", "application/json")
			    				.GET()
			    				.uri(URI.create(urlOrg))
			    				.build();
			    		var client = HttpClient.newBuilder().build();
			    		var response = client.send(request, HttpResponse.BodyHandlers.ofString());
			    		System.out.println(response.body());
			    		String jsonVal = response.body().substring(12, response.body().length()-1);
			    		System.out.println(jsonVal);
			    		  
			    		 JSONArray array = new JSONArray(jsonVal);
			    		 System.out.println(array.length());
			    		 for(int i=0;i<array.length();i++) {
			    			 JSONObject jsonObj = array.getJSONObject(i);
			    			 System.out.println(jsonObj);
			    		 }
			    		 if(array.length()>0) {
			    			 EmailUtil emailUtil = new EmailUtil();
			    			 emailUtil.sendMail();
			    			 stopThread=false;
			    		 }
			        }
			         System.out.println(new Date());
		        }
		    } catch (InterruptedException e) {
		        e.printStackTrace();
				//Comment added for testing purpose.
		    }
		 
	}

}
