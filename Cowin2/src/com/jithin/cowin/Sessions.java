package com.jithin.cowin;

public class Sessions {
	private long center_id;
	private String name;
	/**
	 * @return the center_id
	 */
	public long getCenter_id() {
		return center_id;
	}
	/**
	 * @param center_id the center_id to set
	 */
	public void setCenter_id(long center_id) {
		this.center_id = center_id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return "Centeres [center_id=" + center_id + ", name=" + name + "]";
	}

	
}
